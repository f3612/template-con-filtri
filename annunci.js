fetch("./annunci.json")
.then(response => response.json())
.then(data=>{


    function categoryFilter() {

        let wrapperCategorie = document.querySelector("#wrapperCategories")
        let categorie = Array.from(new Set(data.map(element => element.category)))


        categorie.forEach(el =>{

            let div = document.createElement("div")
            div.classList.add("form-check")
            div.innerHTML=`
            
            <input class="form-check-input" type="radio" name="flexRadioDefault" id="${el}">
            <label class="form-check-label" for="${el}"><p class="text-main">${el}</p>
            </label>
            `

            wrapperCategorie.appendChild(div)



        })
        
    }

    categoryFilter()

    function cardShow(array) {
        let wrapper = document.querySelector("#wrapperCard")
            wrapper.innerHTML= ``
        
            array.forEach(element =>{

            let div= document.createElement("div")
            div.classList.add("card","cardAnnuncio","my-2")
            div.innerHTML=` <img src="https://picsum.photos/300" class="card-img-top img-fluid" alt="immagine prodotto">
                            <div class="card-body">
                            <h5 class="card-title">${element.name}</h5>
                            <p class="card-text">${element.category}</p>
                            <p class="card-text">${element.price} euro</p>
                            
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
            
            `

            wrapper.appendChild(div)
        })
        
        
    }
    cardShow(data)

    function radioFilter() {
        let radioBox = document.querySelectorAll(".form-check-input")

        radioBox.forEach(element=>{

            element.addEventListener("click", ()=>{
                if(element.id == "All"){
                    cardShow(data)
                }else{
                    let filtered = data.filter(el => el.category ==element.id)
                    cardShow(filtered)
                }
            })

        })
        
    }
    radioFilter()



    function price() {
       let prezzoMassimo= data.map(el => Number(el.price)).sort((a,b)=>a-b).pop()

        let price= document.querySelector("#price")
        price.max= Math.ceil(prezzoMassimo)
        price.value= Math.ceil(prezzoMassimo)
        let numberPrice = document.querySelector("#numberPrice")
        numberPrice.innerHTML= `${price.max} euro`

        price.addEventListener("input",()=>{
            numberPrice.innerHTML=` ${price.value} euro`
        })
        
    }
    price()

    function filtraPerPrezzo() {

        let price= document.querySelector("#price")

        price.addEventListener("input",()=>{
            let filtered = data.filter(el => Number(el.price) <= price.value)
            cardShow(filtered)
        })

        
    }
    filtraPerPrezzo()


    function filtraPerTesto() {
        let word = document.querySelector("#wordInput")
        word.addEventListener("input",()=>{
            let filtered = data.filter(el=> el.name.toLowerCase().includes(word.value.toLowerCase()))
            cardShow(filtered)
        })
        
    }
    filtraPerTesto()

})