let nav = document.querySelector("#navjs")
let cards = document.querySelectorAll(".cards")
let icons= document.querySelectorAll(".icone")




document.addEventListener("scroll",()=>{
    console.log(window.scrollY);
    if(window.scrollY > 50 && window.scrollY <700){
        nav.classList.add("d-none")
        
    }else{
        nav.classList.remove("d-none")
    }
    
    
    
})


cards.forEach((el,i)=>{
    el.addEventListener("mouseenter",()=>{
        icons[i].classList.remove("icone")
    })
    
})

cards.forEach((el,i)=>{
    el.addEventListener("mouseleave",()=>{
        icons[i].classList.add("icone")
    })
    
})

// contatore
let utenti= document.querySelector("#nUtenti")
let annunci = document.querySelector("#nAnnunci")

let utenticounter = 0
let annuncicounter = 0

function contatore() {
    
    
    let utentiInterval= setInterval(() => {
        utenticounter++
        utenti.innerHTML=`${utenticounter}`
        if(utenticounter>900){
            clearInterval(utentiInterval)
        }
    }, 10);
    
    let annunciInterval= setInterval(() => {
        annuncicounter++
        annunci.innerHTML=`${annuncicounter}`
        if(annuncicounter>90){
            clearInterval(annunciInterval)
        }
    }, 100);
    
    
    
}

let confirm = true

document.addEventListener("scroll", ()=>{
    
    if(window.scrollY>500 && confirm == true){
        contatore()
        confirm=false
    }
})



// cards

let wrapper = document.querySelector("#wrapper")

let arraydicard=[
    {"title" : "armadio", "category": "arredo"},
    {"title" : "Iphone", "category": "cellulari"},
    {"title" : "asus", "category": "informatica"},
    {"title" : "Ford", "category": "automobili"},
];

function createcard() {
    

        let sliced =arraydicard.slice(-3)

    sliced.forEach((el,i) => {
        let div= document.createElement("div")
        div.classList.add("col-12", "col-md-6", "col-lg-4", "my-5")
        div.innerHTML= `<div class="card" style="width: 18rem;">
        <img class="card-img-top img-fluid" src="https://picsum.photos/300" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">${el.title}</h5>
          <p class="card-text">${el.category}</p>
          <a href="#" class="btn btn-primary">vedi dettagli</a>
        </div>
      </div>`

      wrapper.appendChild(div)


    })
}

createcard()


// btn-up

let up= document.querySelector(".btn-none")

document.addEventListener("scroll",()=>{
    if(window.scrollY>0){
        up.classList.remove("btn-none")
    }else{
        up.classList.add("btn-none")
    }
})

